---
title: "Test of R Markdown"
author: "DC"
date: "6/2/2020"
output: 
  html_document: 
    keep_md: yes
---

```r
## Brilliant code
x <- 1:10
print(x)
```

```
##  [1]  1  2  3  4  5  6  7  8  9 10
```

